mkbd65 CAD files for case and key caps
======================================

_The case  and key set for mkbd65 is still a work in progress!_

This is the case for mkbd65. It is build using [OpenSCAD][openscad] to be able to generate multiple cases fitting
different phones from a single file.
Follow the guide at https://mkbd65.xengi.de to build a case for your phone.

## Generate a case

Edit the file `main.scad` and import your phones scad file from the `lib/` folder. If your phone is not in there it's
not supported yet. Please open a ticket or join the #mkbd65 channel on hackint via [IRC](irc) or [Matrix](matrix) and
ask for support. Adding support for a new phone is pretty easy as long as some requirements are met.

If your phone is supported and you have edited the file, run this command to generate the 3MF and STL files for your 3D
printers slicer software. You can use whatever material you like. PLA has proven to be easy to print and working well
for the case.

```shell
make main
```

## Generate a key set

To generate the key set you can either use a predefined layout or generate your own. There are two predefined layouts
for English US International and German. You can generate the 3D files for them with the following commands:

```shell
make keys_en-us-intl
make keys_de
```

This will generate the neccessary 3MF and STL files that you can use for your 3D printers slicer software. I recommend a
flexible filament like [Filaflex 40](filaflex) for the best typing experience.

### Custom layout

If you want to build your own custom layout follow the instruction in the file `custom_keyset.scad`. It will explain how
to write the openscad code for a key set and generate the 3MF and STL files. You can use any of the keys from a 5x14
matrix and also combine them however you like. Remember that you have to duplicate the keys in the config of the QMK
software for every non 1x1 key too. Because electrically every switch is still a single unique key and gets detected as
such.

So if you want to create a 3x1 space bar at position (0, 5) you can create a key cap by using `key(x=0, y=5, w=3)`. In
the QMK config you have to create 3 distinct space bar keys next to each other.

In theory the maximum width of a key is 3. You can use more then that but the key will get sliced at different points to
avoid accidental duble presses of the switches underneath.

---

[openscad]: https://openscad.org
[filaflex]: https://fila-tech.store/product/filaflexible40
[irc]: ircs://irc.hackint.org:6697/#mkbd65
[matrix]: https://matrix.to/#/#mkbd65:hackint.org

---

**old docs**

Generate CAD files
------------------

1. Install dependencies
   ```shell
   pip3 install --user -r requirements.txt
   ```
2. Generate files for your phone for example the Oneplus 8T
   ```shell
   ./mkbd65.py --layout=ansi --3mf oneplus_8t.ini
   ```

Build instructions
------------------

### 3D printing

Most of the case can be 3D printed using a home 3D printer, any online 3D printing service or at your local hack/make space. You can find your local hack/make space [here](https://wiki.hackerspaces.org/List_of_Hacker_Spaces).

Print out these files with a sturdy filament like PLA:

- `lower_case`

Print out the `key_mat` with a flexible filament like [FilaFlexible40](https://fila-tech.store/product/filaflexible40).

Config files
------------

Properties for different phones are saved in INI files. Currently the only file included is `oneplus_8t.ini` for the
Oneplus 8T phone. You can add your one phone by providing the INI file with measurements and a file describing it's camera bump.

In the config file you can define the phones size but also other aspects like the used font for the key mat. Here is a list of
the possile options:

| Group   | Option    | Type   | Default                | Description                   |
| ------- | --------- | ------ | ---------------------- | ----------------------------- |
| keymap  | thickness | Number | 0.5                    | Thickness of the keymap in mm |
| key_mat | thickness | Number | 1                      | Thickness of the key in mm    |
|         | font      | String | DejaVu Sans:style=Bold | Font used for labels          |

OpenSCAD files
--------------

The generator script will create multiple files.

| File                   | Material | Description                            |
|------------------------| -------- | -------------------------------------- |
| mkbd65_key_mat.scad    | TPU      | 3D printable                           |
| mkbd65_pcb.scad        | -        | Just a spacer for the 3D model fitting |
| mkbd65_top_plate.scad  | Acrylic  | Laser cut on a CNC                     |
| mkbd65_lower_case.scad | PLA      | 3D printable                           |
| mkbd65_internals.scad  | PLA      | 3D printable                           |
| mkbd65.scad            | -        | Combination of the above files         |

---

![CC BY-SA 4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/mkbd65/case">mkbd65 case</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://mkbd65.xengi.de">XenGi</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0</a></p>
