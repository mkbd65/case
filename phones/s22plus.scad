/*
 * Samsung Galaxy S22+
 *
 * Every phone file must expose the following variables and modules:
 * - phone_width: the phone width in mm
 * - phone_length: the phones length in mm
 * - phone_thickness: the phones thickness in mm
 * - phone_corner_rad: the radius of the phones corners in mm
 * - phone_body: module creating the phones body centered around the origin
 * - camera_bump: module creating the phones camera bump to cut it out from the case
 *     
 * If you hold youphone in landscape the longer edge is considered it's width/x axis.
 * The short edge is therefore the length/y axis. The thickness should be self explanatory.
 *
 *    /\
 *   /  \
 *  /    \ x/width
 * /      \
 * |\      \
 *  \\      \
 *   \\     /
 *    \\   /
 *     \\ / y/length
 *      \/
 *      z/thickness
 *
 * The phones main body (w/o camera bump) is centered on the x and y axis. The phones back
 * is flat on the origin and not centered. The camera bump and other things like flah light
 * cone are only in the sectors that have negative z.
 * The bump and flash light cone are used to subtract them from the case.
 */

phone_width = 157.4;
phone_length = 75.8;
phone_thickness = 7.6;
phone_corner_rad = 10;

module phone_body() {
  $fn = 64;
  
  hull() {
    translate([phone_width/2-phone_corner_rad,phone_length/2-phone_corner_rad,0]) cylinder(h=phone_thickness, r=phone_corner_rad);
    translate([-1*(phone_width/2-phone_corner_rad),phone_length/2-phone_corner_rad,0]) cylinder(h=phone_thickness, r=phone_corner_rad);
    translate([phone_width/2-phone_corner_rad,-1*(phone_length/2-phone_corner_rad),0]) cylinder(h=phone_thickness, r=phone_corner_rad);
    translate([-1*(phone_width/2-phone_corner_rad),-1*(phone_length/2-phone_corner_rad),0]) cylinder(h=phone_thickness, r=phone_corner_rad);
  }
}

module camera_bump() {
  $fn = 64;
  
  bump_thickness = 1.5;
  bump_width = 50;
  bump_length = 22;
  
  translate([-phone_width/2, phone_length/2, -bump_thickness]) hull() {
    translate([phone_corner_rad, -phone_corner_rad, 0]) cylinder(h=bump_thickness, r1=phone_corner_rad-1, r2=phone_corner_rad);
    translate([2,-bump_length+1,0]) cylinder(h=bump_thickness, r1=1, r2=2);
    translate([bump_width-phone_corner_rad,-bump_length+phone_corner_rad, 0]) cylinder(h=bump_thickness, r1=phone_corner_rad-1, r2=phone_corner_rad);
    translate([bump_width-2,-2,0]) cylinder(h=bump_thickness, r1=1, r2=2);
  }
  // flashlight
  translate([-phone_width/2+11,phone_length/2-27,-10]) cylinder(h=10, r1=10, r2=3);
}
