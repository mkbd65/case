$fn = 64;

radius = 7;
inner_radius = 5;
pcb_width = 150.7;
pcb_height = 64.1;
margin = 2;
thickness = 4;

key_width = 9;
key_height = 6;
key_padding = 1;
key_margin = 1;

difference() {
  union() {
    // base shape
    hull() {
      cylinder(h=thickness, r=radius);
      translate([pcb_width+2*margin-2*radius,0,0]) cylinder(h=thickness, r=radius);
      translate([0,pcb_height+2*margin-2*radius,0]) cylinder(h=thickness, r=radius);
      translate([pcb_width+2*margin-2*radius,pcb_height+2*margin-2*radius,0]) cylinder(h=thickness, r=radius);
    }
    // nodges
    translate([10, -radius, 0]) cylinder(h=thickness, d=10);
    translate([(pcb_width+2*margin)/2-radius, -radius, 0]) cylinder(h=thickness, d=10);
    translate([pcb_width+2*margin-2*radius-10, -radius, 0]) cylinder(h=thickness, d=10);
    translate([10, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
    translate([(pcb_width+2*margin)/2-radius, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
    translate([pcb_width+2*margin-2*radius-10, pcb_height+2*margin-radius, 0]) cylinder(h=thickness, d=10);
  }
  // threaded inserts
  translate([0,0,thickness-3]) {
    translate([10, -radius-1.5, 0]) cylinder(h=3, d=3);
    translate([(pcb_width+2*margin)/2-radius, -radius-1.5, 0]) cylinder(h=3, d=3);
    translate([pcb_width+2*margin-2*radius-10, -radius-1.5, 0]) cylinder(h=3, d=3);
    translate([10, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=4, d=3);
    translate([(pcb_width+2*margin)/2-radius, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=4, d=3);
    translate([pcb_width+2*margin-2*radius-10, pcb_height+2*margin-radius+1.5, 0]) cylinder(h=3, d=3);
  }
  // usb hole
  translate([82-radius,-radius-10,0]) cube([18, 20, thickness]);
  
  translate([0,0,thickness-1]) hull() {
    cylinder(h=1, r=inner_radius);
    translate([pcb_width-2*inner_radius,0,0]) cylinder(h=1, r=inner_radius);
    translate([0,pcb_height-2*inner_radius,0]) cylinder(h=1, r=inner_radius);
    translate([pcb_width-2*inner_radius,pcb_height-2*inner_radius,0]) cylinder(h=1, r=inner_radius);
  }
  
  translate([0,-radius+margin,thickness-1-1.5]) cube([64,20,1.5]);
  translate([75,-radius+margin,thickness-1-2]) cube([18,20,2]);
  translate([0,0,thickness-1-1]) cube([13*key_width+12*key_padding+2*key_margin, 5*key_height+4*key_padding+2*key_margin, 1]);
}