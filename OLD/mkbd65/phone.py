from solid import OpenSCADObject, linear_extrude, translate, offset, square, cylinder, rotate, hull, cube


def phone(
    width: float,
    height: float,
    thickness: float,
    corner_radius: float,
    side_radius: float
) -> OpenSCADObject:
    """
    Generates rough estimate of the phones shape (basically an oneplus 8t)

    :param width: length from top to bottom (landscape)
    :param height: length from left to right (landscape)
    :param thickness: How thicc is it?
    :param corner_radius: Assuming all phones have round corners
    :param side_radius: Assuming all phones have rounded side rails
    :return:
    """
    return linear_extrude(height=thickness)(
        translate(v=(corner_radius, corner_radius, 0))(
            offset(r=corner_radius)(
                square(size=(width - 2 * corner_radius, height - 2 * corner_radius))
            )
        )
    ) * hull()(
        translate(v=(0, side_radius, side_radius))(
            rotate(a=(0, 90, 0))(
                cylinder(r=side_radius, h=width)
            )
        ),
        translate(v=(0, height - side_radius, side_radius))(
            rotate(a=(0, 90, 0))(
                cylinder(r=side_radius, h=width)
            )
        ),
        translate(v=(0, 0, thickness / 2))(
            cube(size=[width, height, thickness / 2])
        )
    )
