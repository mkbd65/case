from solid import OpenSCADObject, hull, translate, sphere, cube, scale, offset, square, color
from solid.utils import up, right, down, forward

from mkbd65.util import PCB_WIDTH, PCB_HEIGHT, PCB_RADIUS


def lower_case(phone_width: float, phone_height: float, phone_radius: float) -> OpenSCADObject:
    """
    Rounded bottom part of the case

    :param phone_width:
    :param phone_height:
    :param phone_radius:
    :return:
    """
    thickness = 2
    window_thickness = 0.5

    # TODO: make a difference between phone_radius and thickness of side walls

    return color("DarkSlateGray")(up(z=thickness)(
        (scale(v=(1, 1, thickness / phone_radius))(
            hull()(
                translate(v=(phone_radius, phone_radius, 0))(
                    sphere(r=phone_radius),
                    forward(y=phone_height - 2 * phone_radius)(
                        sphere(r=phone_radius)
                    ),
                    right(x=phone_width - 2 * phone_radius)(
                        sphere(r=phone_radius)
                    ),
                    translate(v=(phone_width - 2 * phone_radius, phone_height - 2 * phone_radius, 0))(
                        sphere(r=phone_radius)
                    )
                )
            )
        ) * down(z=thickness)(
            cube([phone_width, phone_height, thickness])
        )) - translate(v=(2 * phone_radius, 2 * phone_radius, -1 * thickness / 2))(
            scale(v=(1, 1, thickness))(
                offset(r=phone_radius)(
                    square([phone_width - 4 * phone_radius, phone_height - 4 * phone_radius])
                )
            )
        ) - translate(v=(phone_width - PCB_WIDTH, phone_radius, -1 * thickness / 2 + window_thickness))(
            scale(v=(1, 1, thickness))(
                offset(r=PCB_RADIUS)(
                    square([PCB_WIDTH - 2 * PCB_RADIUS, PCB_HEIGHT - 2 * PCB_RADIUS])
                )
            )
        )
    ))



# width = 160.7;
# height = 74.1;
# radius = 10;
# thickness = 2;

# pcb_radius = 5;
# pcb_width = 150;
# pcb_height = 64;
# window_thickness = 0.5;
#
#
# translate([0, 0, thickness]) {
#     difference() {
#         intersection() {
#             scale([1, 1, thickness / radius]) {
#                 hull() {
#                     translate([radius, radius, 0]) {
#                         sphere(r=radius);
#                         translate([0, height-2*radius, 0]) {
#                             sphere(r=radius);
#                         }
#                         translate([width-2*radius, 0, 0]) {
#                             sphere(r=radius);
#                         }
#                         translate([width-2*radius, height-2*radius, 0]) {
#                             sphere(r=radius);
#                         }
#                     }
#                 }
#             }
#             translate([0, 0, -thickness]) {
#                 cube([width, height, thickness]);
#             }
#         }
#
#         translate([2*radius, 2*radius, -thickness/2]) {
#           scale([1, 1, thickness]) {
#             offset(r=radius) {
#               square([width - 4 * radius, height - 4 * radius]);
#             }
#           }
#         }
#         translate([width - pcb_width, radius, -1*thickness/2+window_thickness]) {
#           scale([1,1,thickness]) {
#             offset(r=pcb_radius) {
#               square([pcb_width - 2 * pcb_radius, pcb_height - 2 * pcb_radius]);
#             }
#           }
#         }
#     }
# }
